# Démonstrateur SonarQube/Gitlab avec Maven

Cette application illustre l'utilisation du plugin SonarQube pour Maven. 

Les références SonarQube :

- sur Gitlab : https://docs.sonarqube.org/latest/analysis/gitlab-integration/
- Sur Maven : https://docs.sonarqube.org/latest/analysis/scan/sonarscanner-for-maven/

Elle contient deux modules, un backend Java et un frontend simple, pour illustrer l'analyse
de fichiers html. javascript et css par ce plugin.

# Pré-requis SonarQube

- Créer un projet sur Sonarqube
- Générer un jeton d'accès SonarQube autorisé sur le projet (project token par exemple).

Vous disposez alors des éléments SonarQube nécessaires :

- Adresse du serveur SonarQube, dans la variable `SONAR_HOST_URL` par la suite
- Clé du projet SonarQube, dans la variable `SONAR_PROJECT_KEY` par la suite
- Jeton d'accès au projet SonarQube, dans la variable `SONAR_TOKEN` par la suite

# Analyse manuelle

Se positionner à la racine du projet et lancer l'analyse depuis Maven :

```shell
$ mvn verify sonar:sonar -Dsonar.projectKey=$SONAR_PROJECT_KEY -Dsonar.host.url=$SONAR_HOST_URL -Dsonar.login=$SONAR_TOKEN
```
# Analyse via Gitlab

L'analyse via Gitlab n'est que le report de la commande en analyse manuelle
Les variables d'environnement doivent au préalable être définies dans la configuration CI/CD du projet ou l'un de ses groupes parents :

- SONAR_HOST_URL=[Adresse du serveur SonarQube]
- SONAR_TOKEN=[Jeton_SonarQube]
- SONAR_PROJECT_KEY=[Clé_projet_SonarQube]

Le fichier gitlab-ci du projet illustre la mise en œuvre. Il ne contient que l'analyse SonarQube par soucis de simplification.

# Analyse des fichiers frontend par Maven

Le plugin SonarQube Maven est principalement conçu pour les sources Java. Lorsque l'application
contient également des sources de type HTML, Javascript, Typesript ou CSS cela ne suffit pas.

Plusieurs approches sont possibles.

- Séparation des frontend et backend dans deux projets séparés, deux analyses SonarQube distinctes. Le backend est analysé par le plugin Maven SonarQube, le frontend l'est par le scanner SonarQube. C'est une solution simple et efficace.
- Conservation des frontend et backend dans le même projet Maven. Une façon de gérer les sources frontend via Maven
est d'utiliser le plugin frontend-maven-plugin, solution ici retenue (https://github.com/eirslett/frontend-maven-plugin).

Le plugin frontend-maven-plugin ajoute un élément de complexité au projet. 

- Il est souvent utilisé lorsque le frontend est de type SPA, son utilisation pour SonarQube se glisse alors dans la chaîne CI sans difficulté et sans surcoût.
- Lorsque le frontend est de type "server-side", il n'est utile que pour SonarQube. Ce projet montre comment
le configurer pour réduire à minima ses conséquences dans la chaîne de CI :
  - Il n'est déclenché que sur la seule phase d'analyse et non pas à compilation
  - Il ne sert qu'à installer Nodejs et ne lance pas de déploiement de dépendances (~~npm install~~)

