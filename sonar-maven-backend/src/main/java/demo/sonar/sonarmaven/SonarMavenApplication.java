package demo.sonar.sonarmaven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SonarMavenApplication {

    public static void main(String[] args) {
        //On utilise une fonction "Deprecated" pour provoquer une erreur Sonar
        runApplication(args);
    }

    @Deprecated
    static void runApplication(String[] args) {
        SpringApplication.run(SonarMavenApplication.class, args);
    }
}
